import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CVRoutingModule } from './cv-routing.module';
import { CurriculumComponent } from './curriculum/curriculum.component';

@NgModule({
  declarations: [CurriculumComponent],
  exports: [
    CurriculumComponent
  ],
  imports: [
    CommonModule,
    CVRoutingModule
  ]
})
export class CVModule { }
