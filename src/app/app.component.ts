import { Component } from '@angular/core';

@Component({
  selector: 'Coco-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PortfolioCorentin';
}
