import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalToyotaComponent } from './modal-toyota.component';

describe('ModalToyotaComponent', () => {
  let component: ModalToyotaComponent;
  let fixture: ComponentFixture<ModalToyotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalToyotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalToyotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
