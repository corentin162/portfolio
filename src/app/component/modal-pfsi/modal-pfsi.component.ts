import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'Coco-modal-pfsi',
  templateUrl: './modal-pfsi.component.html',
  styleUrls: ['./modal-pfsi.component.scss']
})
export class ModalPFSIComponent  {

  modalRef: BsModalRef;
  constructor(private modalService: BsModalService) {}

  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }
}


