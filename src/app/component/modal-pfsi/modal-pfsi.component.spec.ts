import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPFSIComponent } from './modal-pfsi.component';

describe('ModalPFSIComponent', () => {
  let component: ModalPFSIComponent;
  let fixture: ComponentFixture<ModalPFSIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPFSIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPFSIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
