import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAtpMicroComponent } from './modal-atp-micro.component';

describe('ModalAtpMicroComponent', () => {
  let component: ModalAtpMicroComponent;
  let fixture: ComponentFixture<ModalAtpMicroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAtpMicroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAtpMicroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
