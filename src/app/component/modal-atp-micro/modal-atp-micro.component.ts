import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'Coco-modal-atp-micro',
  templateUrl: './modal-atp-micro.component.html',
  styleUrls: ['./modal-atp-micro.component.scss']
})
export class ModalAtpMicroComponent {

  modalRef: BsModalRef;

  constructor(private modalService: BsModalService) {
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);

  }
}
