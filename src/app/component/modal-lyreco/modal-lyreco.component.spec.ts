import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalLyrecoComponent } from './modal-lyreco.component';

describe('ModalLyrecoComponent', () => {
  let component: ModalLyrecoComponent;
  let fixture: ComponentFixture<ModalLyrecoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalLyrecoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLyrecoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
