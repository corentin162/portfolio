import {Component, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'Coco-modal-lyreco',
  templateUrl: './modal-lyreco.component.html',
  styleUrls: ['./modal-lyreco.component.scss']
})
export class ModalLyrecoComponent {

  modalRef: BsModalRef;
  constructor(private modalService: BsModalService) {}

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
