import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PageComponent} from './container/page/page.component';
import {SocialButtonComponent} from './component/social-button/social-button.component';
import {MailLogoComponent} from './component/mail-logo/mail-logo.component';
import {NgxBootstrapModule} from './ngx-bootstrap/ngx-bootstrap.module';
import {ModalToyotaComponent} from './component/modal-toyota/modal-toyota.component';
import { ModalAtpMicroComponent } from './component/modal-atp-micro/modal-atp-micro.component';
import { ModalPFSIComponent } from './component/modal-pfsi/modal-pfsi.component';
import { ModalLyrecoComponent } from './component/modal-lyreco/modal-lyreco.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    SocialButtonComponent,
    MailLogoComponent,
    ModalToyotaComponent,
    ModalAtpMicroComponent,
    ModalPFSIComponent,
    ModalLyrecoComponent,
    NotFoundComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxBootstrapModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [NgxBootstrapModule]
})
export class AppModule {
}
