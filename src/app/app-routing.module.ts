import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageComponent} from './container/page/page.component';
import {NotFoundComponent} from './not-found/not-found.component';


const routes: Routes = [
  {path: '', redirectTo: 'Acceuil', pathMatch: 'full'},
  {path: 'Acceuil', component: PageComponent},
  {path: 'CV', loadChildren: './cv/cv.module#CVModule'},
   {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
